package Entities;
// Generated Mar 14, 2018 8:29:50 AM by Hibernate Tools 5.2.8.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Shop generated by hbm2java
 */
@Entity
@Table(name = "shop", catalog = "camera")
public class Shop implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer shopId;
	private String name;
	private String address;
	private String phone;
	private Set<Camera> cameras = new HashSet<Camera>(0);
	private Set<Customer> customers = new HashSet<Customer>(0);

	public Shop() {
	}

	public Shop(String name, String address, String phone, Set<Camera> cameras, Set<Customer> customers) {
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.cameras = cameras;
		this.customers = customers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "shop_id", unique = true, nullable = false)
	public Integer getShopId() {
		return this.shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	@Column(name = "name", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "address", length = 200)
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "phone", length = 20)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop")
	public Set<Camera> getCameras() {
		return this.cameras;
	}

	public void setCameras(Set<Camera> cameras) {
		this.cameras = cameras;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop")
	public Set<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

}
