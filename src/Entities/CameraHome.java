package Entities;
// Generated Mar 14, 2018 8:29:55 AM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class Camera.
 * @see Entities.Camera
 * @author Hibernate Tools
 */
@Stateless
public class CameraHome {

	private static final Log log = LogFactory.getLog(CameraHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Camera transientInstance) {
		log.debug("persisting Camera instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Camera persistentInstance) {
		log.debug("removing Camera instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Camera merge(Camera detachedInstance) {
		log.debug("merging Camera instance");
		try {
			Camera result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Camera findById(Integer id) {
		log.debug("getting Camera instance with id: " + id);
		try {
			Camera instance = entityManager.find(Camera.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
