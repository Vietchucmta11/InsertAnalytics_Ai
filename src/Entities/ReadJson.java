package Entities;

public class ReadJson {
	private String link;
	private String gender;
	private int age;
	private String emotion;
	private String customer_id;

	public ReadJson() {
	};

	public ReadJson(String link, String gender, int age, String emotion, String customer_id) {
		this.link = link;
		this.gender = gender;
		this.age = age;
		this.emotion = emotion;
		this.customer_id = customer_id;
	}

	public String getLink() {
		return link;
	}

	public String getGender() {
		return gender;
	}

	public int getAge() {
		return age;
	}

	public String getEmotion() {
		return emotion;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
}
