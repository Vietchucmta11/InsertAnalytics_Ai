package Entities;
// Generated Mar 14, 2018 8:29:50 AM by Hibernate Tools 5.2.8.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Camera generated by hbm2java
 */
@Entity
@Table(name = "camera", catalog = "camera")
public class Camera implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer cameraId;
	private Shop shop;
	private String name;
	private String place;
	private Set<Customer> customers = new HashSet<Customer>(0);

	public Camera() {
	}

	public Camera(Shop shop, String name) {
		this.shop = shop;
		this.name = name;
	}

	public Camera(Shop shop, String name, String place, Set<Customer> customers) {
		this.shop = shop;
		this.name = name;
		this.place = place;
		this.customers = customers;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "camera_id", unique = true, nullable = false)
	public Integer getCameraId() {
		return this.cameraId;
	}

	public void setCameraId(Integer cameraId) {
		this.cameraId = cameraId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_id", nullable = false)
	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "place", length = 200)
	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "camera")
	public Set<Customer> getCustomers() {
		return this.customers;
	}

	public void setCustomers(Set<Customer> customers) {
		this.customers = customers;
	}

}
