package Entities;
// Generated Mar 14, 2018 8:29:55 AM by Hibernate Tools 5.2.8.Final

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Home object for domain model class Shop.
 * @see Entities.Shop
 * @author Hibernate Tools
 */
@Stateless
public class ShopHome {

	private static final Log log = LogFactory.getLog(ShopHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	public void persist(Shop transientInstance) {
		log.debug("persisting Shop instance");
		try {
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void remove(Shop persistentInstance) {
		log.debug("removing Shop instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	public Shop merge(Shop detachedInstance) {
		log.debug("merging Shop instance");
		try {
			Shop result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Shop findById(Integer id) {
		log.debug("getting Shop instance with id: " + id);
		try {
			Shop instance = entityManager.find(Shop.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
}
