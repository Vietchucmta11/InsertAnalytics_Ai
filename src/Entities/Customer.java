package Entities;
// Generated Mar 14, 2018 8:29:50 AM by Hibernate Tools 5.2.8.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Customer generated by hbm2java
 */
@Entity
@Table(name = "customer", catalog = "camera")
public class Customer implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Camera camera;
	private Shop shop;
	private String customerId;
	private String gender;
	private Integer age;
	private String emotion;
	private String imageNew;
	private String imageOld;
	private Date datetimeNew;
	private Date datetimeOld;

	public Customer() {
	}

	public Customer(Camera camera, Shop shop, String customerId, String imageNew, Date datetimeNew) {
		this.camera = camera;
		this.shop = shop;
		this.customerId = customerId;
		this.imageNew = imageNew;
		this.datetimeNew = datetimeNew;
	}

	public Customer(Camera camera, Shop shop, String customerId, String gender, Integer age, String emotion,
			String imageNew, String imageOld, Date datetimeNew, Date datetimeOld) {
		this.camera = camera;
		this.shop = shop;
		this.customerId = customerId;
		this.gender = gender;
		this.age = age;
		this.emotion = emotion;
		this.imageNew = imageNew;
		this.imageOld = imageOld;
		this.datetimeNew = datetimeNew;
		this.datetimeOld = datetimeOld;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "camera_id", nullable = false)
	public Camera getCamera() {
		return this.camera;
	}

	public void setCamera(Camera camera) {
		this.camera = camera;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_id", nullable = false)
	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	@Column(name = "customer_id", nullable = false, length = 20)
	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Column(name = "gender", length = 10)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "age")
	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "emotion", length = 20)
	public String getEmotion() {
		return this.emotion;
	}

	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}

	@Column(name = "image_new", nullable = false, length = 200)
	public String getImageNew() {
		return this.imageNew;
	}

	public void setImageNew(String imageNew) {
		this.imageNew = imageNew;
	}

	@Column(name = "image_old", length = 200)
	public String getImageOld() {
		return this.imageOld;
	}

	public void setImageOld(String imageOld) {
		this.imageOld = imageOld;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datetime_new", length = 19)
	public Date getDatetimeNew() {
		return this.datetimeNew;
	}

	public void setDatetimeNew(Date datetimeNew) {
		this.datetimeNew = datetimeNew;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datetime_old", length = 19)
	public Date getDatetimeOld() {
		return this.datetimeOld;
	}

	public void setDatetimeOld(Date datetimeOld) {
		this.datetimeOld = datetimeOld;
	}

}
