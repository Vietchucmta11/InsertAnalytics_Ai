package main;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class Program {
	public static final String CHANNEL_NAME = "JSON";

	public static void main(String[] arg) throws Exception {
		System.out.println("Bat dau lang nghe!!!");
		final JedisPoolConfig poolConfig = new JedisPoolConfig();
		final JedisPool jedisPool = new JedisPool(poolConfig, "192.168.0.50", 6379, 0);
		final Jedis subscriberJedis = jedisPool.getResource();
		final Subscriber subscriber = new Subscriber();
		try {
			subscriberJedis.subscribe(subscriber, CHANNEL_NAME);
		} catch (Exception e) {
			System.out.println(0);
		}

		subscriber.unsubscribe();
		jedisPool.returnResource(subscriberJedis);
	}
}
