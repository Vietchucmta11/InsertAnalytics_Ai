package Core;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import DaoImpl.CustomerDaoImpl;
import Entities.Camera;
import Entities.Customer;
import Entities.ReadJson;
import Entities.Shop;

public class ReadMessJson {

	public Customer dataCustomer(ReadJson readJson) {
		Customer customer = new Customer();
		Camera camera = new Camera();
		customer.setAge(readJson.getAge());
		customer.setImageNew(readJson.getLink());
		customer.setEmotion(readJson.getEmotion());
		customer.setCustomerId(readJson.getCustomer_id());
		customer.setGender(readJson.getGender());	
		customer.setImageOld(null);
		customer.setDatetimeOld(null);
		
		// lay thong tin anh gan nhat
		try {
			Customer customerOld = new CustomerDaoImpl().getCustomerNext(readJson.getCustomer_id());
			customer.setImageOld(customerOld.getImageNew());
			customer.setDatetimeOld(customerOld.getDatetimeNew());
		} catch (Exception e) {
			customer.setImageOld(null);
			customer.setDatetimeOld(null);
		}
		
//		// lay ma camera
		List<String> listStr = cutLink(readJson.getLink());
//		try {
//			camera = cameraBo.detailCameraByName(listStr.get(1));
//			customer.setCamera(camera);
//		} catch (Exception e) {
//			camera.setCameraId(-1);
//			customer.setCamera(camera);
//		}
		camera.setCameraId(Integer.parseInt(listStr.get(1)));
		customer.setCamera(camera);
		Shop shop=new Shop();
		shop.setShopId(Integer.parseInt(listStr.get(2)));
		customer.setShop(shop);
		// chuyen time Unix
		if (listStr.get(0) != null) {
			customer.setDatetimeNew(parseTimeUnix(listStr.get(0)));
		}
		return customer;
	}

	// doc chuoi json
	public ReadJson readMessJson(String mess) {
		ReadJson readJson = new ReadJson();
		JSONParser parser = new JSONParser();
		try {
			Object obj = parser.parse(mess);
			JSONObject jsonObject = new JSONObject();
			jsonObject = (JSONObject) obj;
			readJson.setLink((String) jsonObject.get("link"));
			readJson.setEmotion((String) jsonObject.get("emotion"));
			readJson.setCustomer_id((String) jsonObject.get("ID"));
			readJson.setGender((String)jsonObject.get("gender"));
			try {
				Long age = (Long) jsonObject.get("age");
				readJson.setAge(age.intValue());
			} catch (Exception e) {
				readJson.setAge(-1);
			}
			return readJson;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	// cat link
	public  List<String> cutLink(String link) {
		try {
			String[] chuoi = link.split("/");
			List<String> listStr = new ArrayList<String>();
			for (int i = chuoi.length - 1; i > 0; i--) {
				listStr.add(chuoi[i]);
			}
			return listStr;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	// chuyen timeUnix parse Date
	public  Date parseTimeUnix(String str) {
		try {
			String[] chuoi = str.split("\\.");
			long fDate = Long.parseLong(chuoi[0].split("\\_")[1]);
			Date date = new Date(fDate * 1000L);
			return date;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
