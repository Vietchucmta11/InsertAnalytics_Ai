package DaoImpl;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import Entities.Camera;
import main.HibernateUtil;

public class CameraDaoImpl {
	public List<Camera> getListCamera() {
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.getCurrentSession();
		Transaction tr = null;
		try {
			tr = session.beginTransaction();
			CriteriaQuery<Camera> cq = session.getCriteriaBuilder().createQuery(Camera.class);
			cq.from(Camera.class);
			List<Camera> cameras = session.createQuery(cq).getResultList();
			tr.commit();
			return cameras;
		} catch (Exception e) {
			// TODO: handle exception
			e.fillInStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public Camera detailCameraByName(String name) {
		return null;
	}
}
