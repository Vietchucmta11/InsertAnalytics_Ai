package DaoImpl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import Entities.Customer;
import main.HibernateUtil;

public class CustomerDaoImpl {

	public int insertAnalyticImage(Customer customer) {
		SessionFactory sessFact = HibernateUtil.getSessionFactory();
		Session session = sessFact.getCurrentSession();
		Transaction tr = null;
		try {
			tr = session.beginTransaction();
			session.save(customer);
			tr.commit();
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public Customer getCustomerNext(String customer_id) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Transaction transaction = null;
		Customer customer = new Customer();
		try {
			transaction = session.beginTransaction();
			CriteriaBuilder buider = session.getCriteriaBuilder();
			CriteriaQuery<Customer> criteriaQuery = buider.createQuery(Customer.class);
			Root<Customer> root = criteriaQuery.from(Customer.class);
			criteriaQuery.select(root);
			criteriaQuery.orderBy(buider.desc(root.get("id")));
			Query<Customer> query = session.createQuery(criteriaQuery);
			List<Customer> customers = query.getResultList();
			customer = customers.get(0);
			transaction.commit();
			return customer;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}

	}
}
